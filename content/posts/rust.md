---
title: "Learning Rust"
slug: "learning-rust"
date: 2019-08-28T11:32:00-03:00
images:
tags: 
  - test
  - rust
---

Code example:

```rust
// this is a comment
fn main() -> Result<(), Error> {
  let x = do_the_thing();
  match x {
    Some(x) => println!("{}", x.get_value()!),
    None => println!("Empty!")
  };
}
```

Another: 

```rust
fn frobnicate() {
  get_future()
    .and_then(|x| x.get_another_future())
    .and_then(|y| panic!("oh noes :crab:"));
}
```
